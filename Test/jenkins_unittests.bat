SET FolderPath=%1
 3 SET ResultsPath=C:\UnitTestResults
 4 SET CoverageHistoryPath=C:\CoverageHistory
 5 
 6 SET NunitPath=%FolderPath%\packages\NUnit.ConsoleRunner.3.5.0\tools
 7 SET ReportUnitPath=%FolderPath%\packages\ReportUnit.1.5.0-beta1\tools
 8 SET OpenCoverPath=%FolderPath%\packages\OpenCover.4.6.519\tools
 9 SET ReportGeneratorPath=%FolderPath%\packages\ReportGenerator.2.4.5.0\tools
10 
11 SET UnitTestProj=%FolderPath%\MyProject.UnitTests\MyProject.UnitTests.csproj
12 
13 REM Recreate Results Folder
14 rd /S /Q %ResultsPath%
15 md %ResultsPath%
16 
17 REM Create coverage history folder if not exists
18 if not exist "%CoverageHistoryPath%" mkdir %CoverageHistoryPath%
19 
20 REM Run Nunit3 for tests, it produces TestResult.xml report
21 %NunitPath%\nunit3-console.exe %UnitTestProj% --result=%ResultsPath%\TestResult.xml
22 
23 REM Get nunit command errorlevel
24 SET NunitError=%ERRORLEVEL%
25 
26 REM Run ReportUnit to create HTML Report from Nunit XML report
27 %ReportUnitPath%\ReportUnit.exe %ResultsPath%\TestResult.xml
28 
29 REM Run OpenCover to create coverage XML report
30 %OpenCoverPath%\OpenCover.Console.exe -register:user -register:user -target:%NunitPath%\nunit3-console.exe -targetargs:%UnitTestProj% -output:%ResultsPath%\opencovertests.xml
31 
32 REM Run ReportGenerator to create coverage HTML report from coverage XML
33 %ReportGeneratorPath%\ReportGenerator.exe -reports:%ResultsPath%\opencovertests.xml -targetDir:%ResultsPath% -historydir:%CoverageHistoryPath%
34 
35 REM Fail if Nunit has found an error on tests
36 exit /b %NunitError%